ts-node (10.9.2+~cs64.13.20-2+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Mon, 10 Mar 2025 02:04:06 +0000

ts-node (10.9.2+~cs64.13.20-2) unstable; urgency=medium

  * Team upload.
  * Add patch for typescript 5.0 compatibility (Closes: #1090026)
  * Bump standards version to 4.7.0 (no changes needed)

 -- Ananthu C V <weepingclown@disroot.org>  Sun, 15 Dec 2024 21:22:10 +0530

ts-node (10.9.2+~cs64.13.20-1) unstable; urgency=medium

  * Team upload
  * New upstream version 10.9.2+~cs64.13.20: update typescript config by
    adding "forceConsistentCasingInFileNames: true" into compiler options

 -- Yadd <yadd@debian.org>  Wed, 27 Mar 2024 07:25:36 +0400

ts-node (10.9.2+~cs64.13.14-1) unstable; urgency=medium

  * Team upload
  * Update lintian override info format in d/source/lintian-overrides
    on line 3-4
  * Update standards version to 4.6.2, no changes needed
  * New upstream version 10.9.2+~cs64.13.14

 -- Yadd <yadd@debian.org>  Sun, 21 Jan 2024 17:51:36 +0400

ts-node (10.9.1+~cs8.8.29-1+apertis1) apertis; urgency=medium

  * Sync updates from debian/bookworm
  * Drop patch 0003-src-Extend-test-timeout-to-60-seconds.patch. Not relevant
    any more to this upstream version

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Thu, 20 Apr 2023 16:55:47 +0530

ts-node (10.9.1+~cs8.8.29-1) unstable; urgency=medium

  * Team upload
  * New upstream version 10.9.1+~cs8.8.29
  * Unfuzz patches
  * Add basic autopkgtest

 -- Yadd <yadd@debian.org>  Sun, 17 Jul 2022 08:11:30 +0200

ts-node (10.8.2+~cs8.8.29-1) unstable; urgency=medium

  * Team upload
  * Embed @tsconfig/node18
  * New upstream version 10.8.2+~cs8.8.29
  * Update copyright
  * Update lintian overrides
  * Clean permissions

 -- Yadd <yadd@debian.org>  Mon, 04 Jul 2022 22:11:04 +0200

ts-node (10.8.1+~cs7.8.22-2) unstable; urgency=medium

  * Team upload
  * Skip network connection during build

 -- Yadd <yadd@debian.org>  Thu, 09 Jun 2022 18:49:28 +0200

ts-node (10.8.1+~cs7.8.22-1) unstable; urgency=medium

  * Team upload
  * Reduce tsc patch
  * Embed @cspotcode/source-map-support (fixes node-lru-cache autopkgtest)

 -- Yadd <yadd@debian.org>  Thu, 09 Jun 2022 18:25:13 +0200

ts-node (10.8.1+~cs7.0.21-1) unstable; urgency=medium

  * Team upload
  * New upstream version 10.8.1+~cs7.0.21
  * Refresh patches
  * Update build
  * Update copyright

 -- Yadd <yadd@debian.org>  Thu, 09 Jun 2022 15:39:56 +0200

ts-node (10.7.0+~cs7.0.20-3) unstable; urgency=medium

  * Team upload
  * Declare compliance with policy 4.6.1
  * Update tsc workarounds (Closes: #1011792)

 -- Yadd <yadd@debian.org>  Fri, 27 May 2022 06:44:46 +0200

ts-node (10.7.0+~cs7.0.20-2) unstable; urgency=medium

  * Team upload
  * Fix @tsconfig-node* install
  * Back to unstable

 -- Yadd <yadd@debian.org>  Thu, 10 Mar 2022 08:42:59 +0100

ts-node (10.7.0+~cs7.0.20-1) experimental; urgency=medium

  * Team upload

  [ Julien Puydt ]
  * Fix d/watch.

  [ Yadd ]
  * Update dependencies: + acorn
  * Embed @tsconfig/node* and v8-compile-cache-lib
  * New upstream version 10.7.0+~cs7.0.20
  * Update copyright
  * Indicate that option --swc is broken
  * Add patches to replace source-map-support fork by node-source-map-support
  * Disable patch (needs ava)
  * Update lintian overrides

 -- Yadd <yadd@debian.org>  Wed, 09 Mar 2022 15:31:48 +0100

ts-node (9.1.1-6) unstable; urgency=medium

  * Team upload
  * Link get-stream during build (Closes: #1005654)
  * Replace test modules by available dependencies

 -- Yadd <yadd@debian.org>  Sun, 13 Feb 2022 14:27:48 +0100

ts-node (9.1.1-5) unstable; urgency=medium

  * Team upload

  [ lintian-brush ]
  * Update pattern for GitHub archive URLs from /<org>/<repo>/tags
    page/<org>/<repo>/archive/<tag> -> /<org>/<repo>/archive/refs/tags/<tag>
  * Update standards version to 4.6.0, no changes needed

  [ Yadd ]
  * Drop build-dependency to node-typescript-types (Closes: #979797)
  * Update fix_compilation.patch (Closes: #995220)
  * Update nodejs dependency to nodejs:any
  * Update lintian overrides

 -- Yadd <yadd@debian.org>  Thu, 09 Dec 2021 08:58:10 +0100

ts-node (9.1.1-4apertis1) apertis; urgency=medium

  * d/patches: Extend test timeout to 60 seconds as build is failing in
    Apertis OBS due to exceeded timeout.

 -- Ariel D'Alessandro <ariel.dalessandro@collabora.com>  Fri, 23 Apr 2021 16:22:14 -0300

ts-node (9.1.1-4apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 22 Apr 2021 15:40:51 +0200

ts-node (9.1.1-4) unstable; urgency=medium

  * Team upload
  * Fix test modules

 -- Xavier Guimard <yadd@debian.org>  Thu, 24 Dec 2020 15:37:11 +0100

ts-node (9.1.1-3) unstable; urgency=medium

  * Team upload
  * Embed some modules for test only
  * Enable part of upstream test
  * Build tsconfig.schema.json and tsconfig.schemastore-schema.json
    + Add dependency to node-json-stable-stringify
  * Add test dependencies to node-axios, node-es-abstract, node-react

 -- Xavier Guimard <yadd@debian.org>  Thu, 24 Dec 2020 15:12:45 +0100

ts-node (9.1.1-2) unstable; urgency=medium

  * Team upload
  * Fix clean
  * Use dh-sequence-nodejs auto install (Closes: #976955)

 -- Xavier Guimard <yadd@debian.org>  Thu, 24 Dec 2020 06:34:33 +0100

ts-node (9.1.1-1) experimental; urgency=medium

  * Add runtime dependencies. (Closes: #976619)
  * New upstream release.
  * Bump std-vers to 4.5.1.
  * Update fix_compilation patch for newer upstream.

 -- Julien Puydt <jpuydt@debian.org>  Thu, 10 Dec 2020 14:02:28 +0100

ts-node (9.0.0-1) experimental; urgency=medium

  * Initial release. (Closes: #974835)

 -- Julien Puydt <jpuydt@debian.org>  Sun, 15 Nov 2020 13:21:46 +0100
